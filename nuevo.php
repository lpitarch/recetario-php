<?php
session_start();
include "lib/config.php";

if(isset($_SESSION['usuario'])){
	header("Location: perfil");
}

$template = $twig->loadTemplate("nuevo.html");


if(isset($_POST['añadir'])){
	include("inc/sql.php");
	$sql = new modelos();
	$error="";//compruebo que el usuario y contraseña sean validos
	if(!isset($_POST['username'])||strlen($_POST['username'])<2){
		$error[0]="Tu nombre de usuario debe tener minimo 2 caracteres";
	}
	if(!isset($_POST['password'])||strlen($_POST['password'])<2){
		$error[1]="Tu contraseña debe tener minimo 2 caracteres";
	}
	if(isset($_POST['password2'])&&$error==""){
		if($_POST['password2']!=$_POST['password']){
			$error[2]="Las contraseñas deben coincidir";
		}
	}
	
	if($error!=""){
		$datos = array(
		'title'=>"Nuevo usuario",
		'titulo'=>"Registrarse",
		'error'=>$error,
		'post'=>$_POST['username']);
		echo $template->render($datos);
		
	}else{
		$userid=$sql->user_id($_POST['username']);//busco el id de usuario mediante el nombre de la sesion
		$datos = array(
		'title'=>"Nuevo usuario",
		'titulo'=>"Registrarse",);
		if($userid!=null){
			//el usuario existe
			$error[3]="Usuario en uso";
			$datos = array(
			'title'=>"Nuevo usuario",
			'titulo'=>"Registrarse",
			'error'=>$error,//paso el error a la plantilla
			'post'=>$_POST['username']);
		}else{
			// el usuario no existe
			include("inc/PasswordHash.php");
			$username=$_POST['username'];
			$userpass="pbkdf2_".create_hash($_POST['password']);// acomodo el formato de la nueva clave al estilo de django
			$newuser= array('username'=> $username,
				'password'=>$userpass,
				'last_login'=>date("Y-m-d H:i:s"),
				'date_joined'=>date("Y-m-d H:i:s"));
			$resultado= $sql->nuevo_usuario($newuser);//añado el usuario
			if($resultado=="1"){// guardo el resultado de la insercion para actuar en la plantilla de una forma u otra
				$datos['exito']=1;
			}else{
				$datos['exito']=0;
			}
			
		}
		if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
			$datos['usuario']=$_SESSION['usuario'];
		}
		echo $template->render($datos);
	}
}else{


	$datos = array(
		'title'=>"Nuevo usuario",
		'titulo'=>"Registrarse",);
	
	
	if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
		$datos['usuario']=$_SESSION['usuario'];
	}
	echo $template->render($datos);
	
}

?>
