<?php
session_start();
include "lib/config.php";
if(!isset($_SESSION['usuario'])){
	header("Location: identificarse");
}
$template = $twig->loadTemplate("borrar.html");
$datos = array(
	'title'=>"Borrar receta",
	'titulo'=>"Borrar receta");
	
include("inc/sql.php");
$sql = new modelos();
$receta=$sql->buscar_id($_GET['id']);// busco la receta que quiero borrar mediante id
$usuario=$sql->user_id($_SESSION['usuario']);//busco el id del usuario que tiene la sesion

if($receta['usuario_id']==$usuario['id']){// si el id del usuario coincide con usuario_id de la receta
	$resultado=$sql->borrar_receta($receta['id']);// ejecuto el borrado
	if($resultado>0){// guardo el resultado de la operacion para actuar en la plantilla de una forma u otra
		$datos['exito']=1;
	}else{
		$datos['exito']=0;
	}
}else{
	$datos['exito']=2;
}

if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
	$datos['usuario']=$_SESSION['usuario'];
}
echo $template->render($datos);
?>
