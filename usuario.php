<?php
session_start();
include "lib/config.php";

if(!isset($_SESSION['usuario'])){
	header("Location: identificarse");
}
$template = $twig->loadTemplate("usuario.html");

include("inc/sql.php");
$sql = new modelos();
//busco la id del usuario en la tabla de usuarios mediante el nombre
$userid=$sql->user_id($_SESSION['usuario']);
// busco las recetas en las que el usuario_id coincida con el id del usuario de la sesion
$recetas= $sql->recetas_usuario($userid['id']);
if ($recetas==null){
	$recetas=0;
}
$datos = array(
'title'=>"Perfil",
'titulo'=>"Recetas de ".$_SESSION['usuario'],
'recetas'=>$recetas);

if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
	$datos['usuario']=$_SESSION['usuario'];
}
echo $template->render($datos);

?>
