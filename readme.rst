Recetario con PHP
-------------------- 

Funcionalidad de la aplicación
###############################

Esta aplicacion realizada con php sirve para que cada usuario se registre y comparta recetas con los demás usuarios. Utiliza la misma base de datos que el recetario django

Instrucciones de uso
#####################

En la página principal nos muestra las ultimas recetas añadidas.
En recetas, muestra la lista completa ordenadas por ultimas modificaciones.
Tenemos la opcion de registrarnos con usuario y contraseña. La contraseña utiliza el mismo tipo de encriptación que django
Una vez logeados podemos añadir nuestras recetas y gestionarlas después. Cada usuario solo gestiona sus recetas.


Como instalar
##############

En la raíz de nuestro proyecto ejecutaremos:

  $ curl -s http://getcomposer.org/installer | php
  
  $ php composer.phar install
 
Este proyecto utiliza urls amigables por lo tanto el servidor apache tendrá que estar configurado para tal efecto:

  $ sudo a2enmod rewrite


Para poner en marcha la aplicacion:
====================================

Esta aplicacion utiliza y puede compartir la base de datos del recetario de django. 
La ruta de la base de datos esta configurada para que esté en ../recetario-django/db.sqlite3

Podremos acceder a la aplicación utilizando un navegador web e introduciendo:  localhost/recetario-php