<?php
session_start();
include "lib/config.php";

$template = $twig->loadTemplate("recetas.html");

include("inc/sql.php");
$sql = new modelos();
$recetas= $sql->listar("fecha_modificacion");//guardo las recetas ordenadas por fecha de creacion

$datos = array(
	'title'=>"Recetas",
	'titulo'=>"Ver todas las recetas",
	'recetas'=>$recetas);

if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
	$datos['usuario']=$_SESSION['usuario'];
}
echo $template->render($datos);



?>
