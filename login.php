<?php
session_start();
include "lib/config.php";
if(isset($_SESSION['usuario'])){
	header("Location: perfil");
}

$template = $twig->loadTemplate("login.html");

$msg=null;
if (isset($_POST["enviar"])){
	// me guardo el usuario y la contraseña introducidos
	$nombre=$_POST["usuario"]; 
	$password=$_POST["password"];
	
	// busco la password del usuario en la base de datos y le cambio el formato
	include ("inc/sql.php");
	$sql = new modelos();
	$pwd_django = $sql->buscar_password($nombre);
	$pwd_django = $pwd_django[0]['password'];
	$pwd_django = str_replace('pbkdf2_', '', $pwd_django);
	$pwd_django = str_replace("$", ":", $pwd_django);
	
	// comparo la password introducida guardada en la variable con la que tenía en la base de datos
	include ("inc/PasswordHash.php");
	if(validate_password($password, $pwd_django)){
		$_SESSION['usuario']=$nombre;
		var_dump($_SESSION['usuario']);
		header("Location: perfil");
	}else{
		$msg= "Usuario o contraseña incorrectos";
	}
}

$datos = array(
	'title'=>"Login",
	'titulo'=>"Identificarse",
	'msg'=>$msg);

echo $template->render($datos);



?>
