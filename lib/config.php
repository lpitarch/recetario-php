<?php

// datos de configuracion base de datos

// datos del administrador
$root= "root";
$passwd_admin = "root";

// configuracion twig (realpath nos da la ruta absoluta a este directorio)
require_once realpath(dirname(__FILE__)."/../vendor/twig/twig/lib/Twig/Autoloader.php");

Twig_autoloader::register(); // llamada a metodo estatico
$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__)."/../vistas")); // indicar donde estan las vistas

$twig = new Twig_Environment($loader);


?>