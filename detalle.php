<?php
include "lib/config.php";
session_start();

$template = $twig->loadTemplate("detalle.html");

include("inc/sql.php");
$sql = new modelos();

if(!isset($_GET['id'])){
	header("Location: ./");
}else{
	$id=$_GET['id'];
}
$receta= $sql->buscar_id($id);//guardo las recetas ordenadas por fecha de creacion
$id_user=$receta['usuario_id'];// guardo la id_usuario de la receta
$nombre= $sql->buscar_nom_usuario($id_user); // busco el nombre del usuario mediante su id

$datos = array(
	'title'=>"Recetas",
	'titulo'=>"Detalle de receta",
	'nombre'=>$nombre,
	'receta'=>$receta);
	
if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
	$datos['usuario']=$_SESSION['usuario'];
}
echo $template->render($datos);



?>
