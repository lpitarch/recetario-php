<?php
session_start();
include "lib/config.php";

$template = $twig->loadTemplate("inicio.html");

include("inc/sql.php");
$sql = new modelos();
$recetas= $sql->listar("fecha_creacion",5);//guardo las recetas ordenadas por fecha de creacion y muestro las 5 ultimas

$datos = array(
	'title'=>"Inicio",
	'titulo'=>"Inicio",
	'recetas'=>$recetas);
if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
	$datos['usuario']=$_SESSION['usuario'];
}
echo $template->render($datos);



?>
