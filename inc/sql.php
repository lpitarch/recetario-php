<?php

class modelos{
	
	public function listar($order="",$limit=""){//Listado normal de todas las recetas pudiendo indicar orden de columna y numero maximo de resultados a mostrar
		try{
			$conn = new PDO ('sqlite:../recetario-django/db.sqlite3');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
			
			$listado = "SELECT * from apprecetas_receta";
			if ($order!=""){
				$listado=$listado." order by $order DESC";
			}
			if ($limit!=""){
				$listado=$listado." limit $limit";
			}
			$resultado = $conn->query($listado);
			//compruebo que haya recetas para mostrar
			$resultado = $resultado->fetchAll();
			if(count($resultado)!=0){
				return $resultado;
			}else{
				return null;
			}
		}catch(PDOException $e){
			echo $e->getMessage();
		}
		$conn = null;
	}
	public function insertar_datos($receta,$accion=""){//añadir o modificar recetas
			
		try{	
			$conn = new PDO ('sqlite:../recetario-django/db.sqlite3');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
			
			if ($accion=="añadir"){//si lo que quiero es añadir un contacto nuevo
					
				$insertar = "INSERT into apprecetas_receta(titulo,ingredientes,consejos,preparacion,fecha_creacion,fecha_modificacion,tiempo,dificultad,tipo,imagen,usuario_id)
				
										VALUES(:titulo, :ingredientes, :consejos, :preparacion, :fecha_creacion, :fecha_modificacion, :tiempo, :dificultad, :tipo, :imagen, :usuario_id)";
				
				//proteccion contra inyeccion de ataques sql
				$sentencia = $conn->prepare($insertar);
				$sentencia->execute($receta);
				$msg="1";
			}else if($accion=="modificar"){// si quiero modificar un contacto
				$actualizar = "UPDATE apprecetas_receta SET 
						titulo= :titulo, ingredientes= :ingredientes, consejos= :consejos, preparacion= :preparacion, fecha_modificacion= :fecha_modificacion, tiempo= :tiempo, dificultad= :dificultad, tipo= :tipo, imagen= :imagen
			 			where id = :id";
				//proteccion contra inyeccion de ataques sql
				$sentencia = $conn->prepare($actualizar);
				$sentencia->execute($receta);
				$resultado = $sentencia->rowCount();//recojo resultados
				//compruebo que las filas afectadas sean mas de 0
				if(count($resultado)!=0){
					return $resultado;
				}else{
					return null;
				}
			}
		}catch(PDOException $e){
			return $e->getMessage();
		}
		$conn = null;
		return $msg;				
		
	}
	
	public function nuevo_usuario($usuario){//registrar un nuevo usuario
		try{	
			$conn = new PDO ('sqlite:../recetario-django/db.sqlite3');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
			
			$insertar = "INSERT into auth_user(username,password,last_login,is_superuser,is_staff,is_active,date_joined,first_name,last_name,email)
									VALUES(:username, :password, :last_login, 0, 0, 1, :date_joined, ' ' , ' '  ,' ' )";
			
			//proteccion contra inyeccion de ataques sql
			$sentencia = $conn->prepare($insertar);
			$resultado=$sentencia->execute($usuario);
			$msg="1";
		}catch(PDOException $e){
			return $e->getMessage();
		}
		$conn = null;
		return $msg;				
		
	}
	public function buscar_id($id){// buscar la receta a partir de un id
		try{
			$conn = new PDO ('sqlite:../recetario-django/db.sqlite3');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
			$listado = "SELECT * from apprecetas_receta where id = :id";// escribo la consulta
			$id= array('id' => $id );// preparo un array con el id de la receta
			
			//preparo la consulta
			$consulta = $conn->prepare($listado, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$consulta->execute($id);// ejecuto la consulta con el parametro id
			$resultado = $consulta->fetch(PDO::FETCH_ASSOC);//recojo resultados
			//compruebo que la lista tenga recetas que mostrar	
			if(count($resultado)!=0){
				return $resultado;
			}else{
				return null;
			}
		}catch(PDOException $e){
			$conn = null;
			return $e->getMessage();
		}
		$conn = null;
		return $resultado;
	}
	public function buscar_imagen($id){// buscar el nombre de la imagen a partir de un id
		try{
			$conn = new PDO ('sqlite:../recetario-django/db.sqlite3');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
			$listado = "SELECT imagen from apprecetas_receta where id = :id";// escribo la consulta
			$id= array('id' => $id );// preparo un array con el id de la receta
			
			//preparo la consulta
			$consulta = $conn->prepare($listado, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$consulta->execute($id);// ejecuto la consulta con el parametro id
			$resultado = $consulta->fetch(PDO::FETCH_ASSOC);//recojo resultados
			//compruebo que la lista tenga recetas que mostrar	
			if(count($resultado)!=0){
				return $resultado;
			}else{
				return null;
			}
		}catch(PDOException $e){
			$conn = null;
			return $e->getMessage();
		}
		$conn = null;
		return $resultado;
	}
	public function buscar_nom_usuario($id=""){// buscar el nombre de usuario a partir de su id
		try{
			$conn = new PDO ('sqlite:../recetario-django/db.sqlite3');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
			$listado = "SELECT username from auth_user where id = :id";// escribo la consulta
			$id= array('id' => $id );// preparo un array con el id del usuario
			
			//preparo la consulta
			$consulta = $conn->prepare($listado, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$consulta->execute($id);// ejecuto la consulta con el parametro id
			$resultado = $consulta->fetch(PDO::FETCH_ASSOC);//recojo resultados
			//compruebo que exista el usuario que busco
			if(count($resultado)!=0){
				return $resultado;
			}else{
				return null;
			}
		}catch(PDOException $e){
			$conn = null;
			return $e->getMessage();
		}
		$conn = null;
		return $resultado;
	}
	public function user_id($nombre){//buscar el id de usuario a partir de su nombre
		try{
			$conn = new PDO ('sqlite:../recetario-django/db.sqlite3');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
			$listado = "SELECT id from auth_user where username = :username";// escribo la consulta
			$nombre= array('username' => $nombre );// preparo un array con nombre del usuario
			
			//preparo la consulta
			$consulta = $conn->prepare($listado, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$consulta->execute($nombre);
			$resultado = $consulta->fetch(PDO::FETCH_ASSOC);//recojo resultados
			//compruebo que la lista tenga recetas que mostrar	
			if(count($resultado)!=0){
				return $resultado;
			}else{
				return null;
			}
		}catch(PDOException $e){
			$conn = null;
			return $e->getMessage();
		}
		$conn = null;
		return $resultado;
	}
	public function recetas_usuario($id){// buscar las recetas de un determinado usuario
		try{
			$conn = new PDO ('sqlite:../recetario-django/db.sqlite3');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
			$listado = "SELECT * from apprecetas_receta where usuario_id = :usuario_id ORDER BY fecha_modificacion DESC";// escribo la consulta
			$usuario_id = array('usuario_id' => $id );// preparo un array con el id del usuario
			
			//preparo la consulta
			$consulta = $conn->prepare($listado, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$consulta->execute($usuario_id);// ejecuto la consulta con el parametro id
			$resultado = $consulta->fetchAll();//recojo resultados
			//compruebo que el usuario tenga recetas
			if(count($resultado)!=0){
				return $resultado;
			}else{
				return null;
			}
		}catch(PDOException $e){
			$conn = null;
			return $e->getMessage();
		}
		$conn = null;
		return $resultado;
	}
	public function borrar_receta($id){// borrar una receta mediante su id
		try{
			$conn = new PDO ('sqlite:../recetario-django/db.sqlite3');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
			$listado = "Delete from apprecetas_receta where id = :id";// escribo la consulta
			$id= array('id' => $id );// preparo un array con el id de la receta
			
			//preparo la consulta
			$consulta = $conn->prepare($listado, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$consulta->execute($id);// ejecuto la consulta con el parametro id
			$resultado = $consulta->rowCount();//recojo resultados
			//compruebo que las filas afectadas sean mas de 0
			if(count($resultado)!=0){
				return $resultado;
			}else{
				return null;
			}
		}catch(PDOException $e){
			$conn = null;
			return $e->getMessage();
		}
		$conn = null;
		return $resultado;
	}
	public function buscar_password($username){// buscar el password de un usuario para compararlo posteriormente
		try{
			$conn = new PDO ('sqlite:../recetario-django/db.sqlite3');
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
			$listado = "SELECT password from auth_user where username = :username";// escribo la consulta
			$username= array('username' => $username );// preparo un array con el nombre del usuario
			
			//preparo la consulta
			$consulta = $conn->prepare($listado, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$consulta->execute($username);// ejecuto la consulta con el parametro id
			$resultado = $consulta->fetchAll();//recojo resultados
			//compruebo que la lista tenga recetas que mostrar	
			if(count($resultado)!=0){
				return $resultado;
			}else{
				return null;
			}
		}catch(PDOException $e){
			$conn = null;
			return $e->getMessage();
		}
		$conn = null;
		return $resultado;
	}
	
}
?>