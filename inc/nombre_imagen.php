<?php

		// Opciones
      	$allowed_filetypes = array('.jpg','.gif','.bmp','.png'); // extensiones permitidas.
      	$max_filesize = 2048288; // tamaño maximo de imagen 2mb.
      	$upload_path = 'recetas/'; // directorio de las imagenes
      	$filename = $_FILES['imagen']['name']; // cojo el nombre del archivo
	    $ext = substr($filename, strpos($filename,'.'), strlen($filename)-1); // Get the extension from the filename.
	 	
	    // comprobamos que el archivo esta permitido
	    if(!in_array($ext,$allowed_filetypes))
	      	die('The file you attempted to upload is not allowed.');
	 
	    // Comprobamos el tamaño
	    if(filesize($_FILES['imagen']['tmp_name']) > $max_filesize)
	   		die('The file you attempted to upload is too large.');
	 
	    // Comprobamos que el directorio tenga permisos de escritura
	    if(!is_writable($upload_path))
	      	die('You cannot upload to the specified directory, please CHMOD it to 777.');
	 	
		// Cambiamos el nombre del archivo si ya existe
		$actual_name = pathinfo($filename,PATHINFO_FILENAME);
		$original_name = $actual_name;
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$i = 1;
		while(file_exists($upload_path.$actual_name.".".$extension))
		{           
		    $actual_name = (string)$original_name.$i;
		    $name = $actual_name.".".$extension;
		    $i++;
		}
		$filename=$actual_name.".".$extension;//nombre definitivo
?>