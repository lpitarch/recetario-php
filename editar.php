<?php
session_start();
include "lib/config.php";
if(!isset($_SESSION['usuario'])){
	header("Location: identificarse");
}
if(!isset($_POST['id'])&&!isset($_GET['id'])){
	header("Location: perfil");
}
$template = $twig->loadTemplate("editar.html");
$datos = array(
	'title'=>"Editar receta",
	'titulo'=>"Editar receta");
	
include("inc/sql.php");
$sql = new modelos();

if (isset($_GET['id'])){
	$receta=$sql->buscar_id($_GET['id']);// Si pulso en editar receta, su id me viene por get
}elseif(isset($_POST['id'])){
	$receta=$sql->buscar_id($_POST['id']);// Si pulso para modificar en la base de datos, su id me viene por post
}
$usuario=$sql->user_id($_SESSION['usuario']);// Busco el id del usuario que tiene la sesion


if($receta['usuario_id']==$usuario['id']){// si el id del usuario coincide con usuario_id de la receta
	
	if(isset($_POST['editar'])){
		
		$error="";//compruebo que haya al menos un nombre de receta, ingredientes y preparacion
		if(!isset($_POST['titulo'])||strlen($_POST['titulo'])<2){
			$error[0]="Debes añadir el nombre de la receta, minimo 2 caracteres";
		}
		if(!isset($_POST['ingredientes'])||strlen($_POST['ingredientes'])<2){
			$error[1]="Debes añadir los ingredientes, minimo 2 caracteres";
		}
		if(!isset($_POST['preparacion'])||strlen($_POST['preparacion'])<2){
			$error[2]="Debes añadir la preparacion, minimo 2 caracteres";
		}
		if($error!=""){
		$datos = array(
		'title'=>"Editar receta",
		'titulo'=>"Editar receta",
		'error'=>$error,
		'receta'=>$_POST);
		echo $template->render($datos);
			
		}else{
			
			if(isset($_POST['borrarimagen'])){// si he marcado la casilla para borrar la imagen pongo la imagen por defecto
				$ruta="recetas/no.jpg";
			}else{// si no busco la ruta en la base de datos y guardo la misma
				$origen = $sql->buscar_imagen($_POST['id']); 
				$ruta=$origen['imagen'];
			}
			/*
			 * si tengo una imagen en el formulario, guardare esa imagen aunque tenga marcada la casilla de borrar imagen
			 * */
			if ($_FILES["imagen"]["name"]!=""){ // si hay imagen en el formulario
				include ("inc/nombre_imagen.php");// compruebo que no exista, si no le cambio el nombre.
				
				// guardamos el archivo
			    if(move_uploaded_file($_FILES['imagen']['tmp_name'],$upload_path . $filename)){
			    $ruta =  $upload_path . $filename; // me guardo la carpeta y el nombre para guardarlo en la bbdd
			 	}else{
					$mens="Error al subir la imagen";
			    }
			    
			}
			$userid=$sql->user_id($_SESSION['usuario']);//busco el id de usuario mediante el nombre de la sesion
			$userid=$userid['id'];// como me devuelve una array con todo me guardo solo la id
			$receta= array(
				'id'=>$_POST['id'],
				'titulo'=>$_POST["titulo"],
				'ingredientes'=>$_POST["ingredientes"],
				'preparacion'=>$_POST["preparacion"],
				'consejos'=>$_POST["consejos"],
				'fecha_modificacion'=>date("Y-m-d H:i:s"),
				'tiempo'=>$_POST["tiempo"],
				'dificultad'=>$_POST["dificultad"],
				'tipo'=>$_POST["tipo"],
				'imagen'=>$ruta
			);
			
			$resultado= $sql->insertar_datos($receta,"modificar");//añado la receta
			$datos = array(
			'title'=>"Editar receta",
			'titulo'=>"Editar receta");
			if($resultado=="1"){// guardo el resultado de la insercion para actuar en la plantilla de una forma u otra
				$datos['exito']=1;
			}else{
				$datos['exito']=0;
			}
			if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
				$datos['usuario']=$_SESSION['usuario'];
			}
			echo $template->render($datos);
		}
	}else{
		$receta=$sql->buscar_id($_GET['id']);// busco la receta que quiero borrar mediante id
		
		$datos['receta']=$receta;
		if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
			$datos['usuario']=$_SESSION['usuario'];
		}
		echo $template->render($datos);
	}

}else{
	$datos['exito']=2;
	if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
		$datos['usuario']=$_SESSION['usuario'];
	}
	echo $template->render($datos);
}
?>
