<?php
session_start();
include "lib/config.php";

if(!isset($_SESSION['usuario'])){
	header("Location: identificarse");
}

$template = $twig->loadTemplate("nueva.html");


if(isset($_POST['añadir'])){
	include("inc/sql.php");
	$sql = new modelos();
	$error="";//compruebo que haya al menos un nombre de receta, ingredientes y preparacion
	if(!isset($_POST['titulo'])||strlen($_POST['titulo'])<2){
		$error[0]="Debes añadir el nombre de la receta, minimo 2 caracteres";
	}
	if(!isset($_POST['ingredientes'])||strlen($_POST['ingredientes'])<2){
		$error[1]="Debes añadir los ingredientes, minimo 2 caracteres";
	}
	if(!isset($_POST['preparacion'])||strlen($_POST['preparacion'])<2){
		$error[2]="Debes añadir la preparacion, minimo 2 caracteres";
	}
	if($error!=""){
	$datos = array(
	'title'=>"Nueva receta",
	'titulo'=>"Nueva receta",
	'error'=>$error,
	'post'=>$_POST);
	echo $template->render($datos);
		
	}else{
		$ruta = "recetas/no.jpg";// ruta por defecto si no hay imagen
		if ($_FILES["imagen"]["name"]!=""){ // si hay imagen
			include ("inc/nombre_imagen.php");// compruebo que no exista, si no le cambio el nombre.
			
			// guardamos el archivo
		    if(move_uploaded_file($_FILES['imagen']['tmp_name'],$upload_path . $filename)){
		    $ruta =  $upload_path . $filename; // me guardo la carpeta y el nombre para guardarlo en la bbdd
		 	}else{
				$mens="Error al subir la imagen";
		    }
		    
		}
		$userid=$sql->user_id($_SESSION['usuario']);//busco el id de usuario mediante el nombre de la sesion
		$userid=$userid['id'];// como me devuelve una array con todo me guardo solo la id
		$receta= array(
			'titulo'=>$_POST["titulo"],
			'ingredientes'=>$_POST["ingredientes"],
			'preparacion'=>$_POST["preparacion"],
			'consejos'=>$_POST["consejos"],
			'fecha_creacion'=>date("Y-m-d H:i:s"),
			'fecha_modificacion'=>date("Y-m-d H:i:s"),
			'tiempo'=>$_POST["tiempo"],
			'dificultad'=>$_POST["dificultad"],
			'tipo'=>$_POST["tipo"],
			'imagen'=>$ruta,
			'usuario_id'=>$userid
		);
		
		$resultado= $sql->insertar_datos($receta,"añadir");//añado la receta
		$datos = array(
		'title'=>"Nueva receta",
		'titulo'=>"Nueva receta");
		if($resultado=="1"){// guardo el resultado de la insercion para actuar en la plantilla de una forma u otra
			$datos['exito']=1;
			
		}else{
			$datos['exito']=0;
		}
		if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
			$datos['usuario']=$_SESSION['usuario'];
		}
		echo $template->render($datos);
	}
}else{


	$datos = array(
		'title'=>"Nueva receta",
		'titulo'=>"Nueva receta");
	
	
	if(isset($_SESSION['usuario'])&&$_SESSION['usuario']!=""){//si tengo sesion creada la paso a la plantilla
		$datos['usuario']=$_SESSION['usuario'];
	}
	echo $template->render($datos);
	
}

?>
